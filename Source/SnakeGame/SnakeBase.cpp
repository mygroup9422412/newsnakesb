// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include"PlayerPawnBase.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f; // ��������� ���� ��������
	MovementSpeed = 10.f;//��������� ��������
	LastMoveDirection = EMovementDirection::LEFT; //��������� ����������� ��������
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);// �������� ���� �� ������
	AddSnakeElement(4);//������ ���� �� ������
	HowMuchFoodEaten = 0;
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick( DeltaTime);
	Move();//������� ��������
	SnakeHeadLocat = SnakeElements[0]->GetActorLocation();
}
void ASnakeBase::DecriseSnakeElement()
{
	if (SnakeElements.Num() > 0)
	{
		ASnakeElementBase* LastElement = SnakeElements[SnakeElements.Num() - 1];
		SnakeElements.RemoveSingle(LastElement);
		LastElement->Destroy();
	}
}
// ���� ���������
void ASnakeBase::AddSnakeElement(int ElementsNum) // ���������� ������� ���������� ��������
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0); // ���������  ���������� �������� �� �������
		FTransform NewTransform(NewLocation);// ����������� 
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		SnakeElements.Add(NewSnakeElem);//���������� �������� � ������
		int32 ElementIndex = SnakeElements.Find(NewSnakeElem); // ����� �������� ������
		if (ElementIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();

		}
	}

}

 void ASnakeBase::Move() // ��� �������, � ����� �������� ��������
{
	FVector MovementVector;

	switch(LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	case EMovementDirection::UPPER:
		MovementVector.Z += ElementSize;
		break;
	case EMovementDirection::DOWNER:
		MovementVector.Z -= ElementSize;
		break;
	}




	
	//SnakeHeadBoostLocat = (SnakeElements[0]->GetActorLocation() + ElementSize * 10);

	
	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i-1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}


void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement,ElemIndex);
			bool bIsFirst = ElemIndex==0;
			IInteractable* InteractableInterface = Cast<IInteractable>(Other);
			if (InteractableInterface)
			{
				InteractableInterface->Interact(this, bIsFirst);
				auto Pawn = Cast<APlayerPawnBase>(GetOwner());
			}
			Other->Destroy();
	}

}

FVector ASnakeBase::SnakeDash()
{
	FVector SnakeDashLocat = SnakeHeadLocat;
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		SnakeDashLocat.X = SnakeHeadLocat.X + ElementSize * 5;
		return SnakeDashLocat;
	case EMovementDirection::DOWN:
		SnakeDashLocat.X = SnakeHeadLocat.X - ElementSize * 5;
		return SnakeDashLocat;
	case EMovementDirection::LEFT:
		SnakeDashLocat.Y = SnakeHeadLocat.Y + ElementSize * 5;
		return SnakeDashLocat;
	case EMovementDirection::RIGHT:
		SnakeDashLocat.Y = SnakeHeadLocat.Y - ElementSize * 5;
		return SnakeDashLocat;
	case EMovementDirection::UPPER:
		SnakeDashLocat.Z = SnakeHeadLocat.Z + ElementSize * 5;
		return SnakeDashLocat;
	case EMovementDirection::DOWNER:
		SnakeDashLocat.Z = SnakeHeadLocat.Z - ElementSize * 5;
		return SnakeDashLocat;
	}
	return SnakeDashLocat;
}

void ASnakeBase::PlusValueHowMuchFoodEaten()
{
	HowMuchFoodEaten +=1;

}

int ASnakeBase::Geteaten()
{
	
	return HowMuchFoodEaten;
}

