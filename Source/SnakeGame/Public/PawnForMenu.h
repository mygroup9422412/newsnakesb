// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PawnForMenu.generated.h"
class UCameraComponent;
class ASnakeBase;
class ASnakeElementBase;
class AFood;

class ASnakeElementBase;
UCLASS()
class SNAKEGAME_API APawnForMenu : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APawnForMenu();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCameraMenu;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActorMenu;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorMenuClass;



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeMenuActor();

	UFUNCTION()
	void HandlePlayerVerticalInput(float value);

	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);

	UFUNCTION()
	void HandlePlayerUpDownInput(float value);

};
