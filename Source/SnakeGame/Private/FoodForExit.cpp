// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodForExit.h"
#include "Kismet/KismetSystemLibrary.h"
#include "SnakeGame\SnakeBase.h"
// Sets default values
AFoodForExit::AFoodForExit()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodForExit::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodForExit::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodForExit::Interact(AActor* Inteactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Inteactor);
		APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
		if (PlayerController)
		{
			PlayerController->ConsoleCommand("quit");
		}
	}
}

