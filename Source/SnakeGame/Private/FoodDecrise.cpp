// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodDecrise.h"
#include "SnakeGame\SnakeBase.h"
#include"PlayerPawnBase.generated.h"
// Sets default values
AFoodDecrise::AFoodDecrise()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodDecrise::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodDecrise::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodDecrise::Interact(AActor* Inteactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Inteactor);
		if (IsValid(Snake))
		{
			Snake->DecriseSnakeElement();
		}
	}

}