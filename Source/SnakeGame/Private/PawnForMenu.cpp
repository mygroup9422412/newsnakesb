// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnForMenu.h"
#include "Camera/CameraComponent.h"
#include "SnakeGame\SnakeBase.h"
#include "Food.h"
#include "SnakeGame\SnakeElementBase.h"
#include "Components/InputComponent.h"
#include "Math/UnrealMathUtility.h"
#include "FoodDecrise.h"
// Sets default values
APawnForMenu::APawnForMenu()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnCameraMenu = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCameraMenu"));
	RootComponent = PawnCameraMenu;
}

// Called when the game starts or when spawned
void APawnForMenu::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeMenuActor();
}

// Called every frame
void APawnForMenu::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APawnForMenu::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APawnForMenu::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APawnForMenu::HandlePlayerHorizontalInput);
	PlayerInputComponent->BindAxis("Deep", this, &APawnForMenu::HandlePlayerUpDownInput);

}

void APawnForMenu::CreateSnakeMenuActor()
{
	SnakeActorMenu = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorMenuClass, FTransform());
}

void APawnForMenu::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActorMenu))
	{
		if (value > 0 && SnakeActorMenu->LastMoveDirection != EMovementDirection::DOWN)
		{
			SnakeActorMenu->LastMoveDirection = EMovementDirection::UP;
		}
		else if (value < 0 && SnakeActorMenu->LastMoveDirection != EMovementDirection::UP)
		{
			SnakeActorMenu->LastMoveDirection = EMovementDirection::DOWN;
		}
	}
}

void APawnForMenu::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActorMenu))
	{
		if (value > 0 && SnakeActorMenu->LastMoveDirection != EMovementDirection::LEFT)
		{
			SnakeActorMenu->LastMoveDirection = EMovementDirection::RIGHT;
		}
		else if (value < 0 && SnakeActorMenu->LastMoveDirection != EMovementDirection::RIGHT)
		{
			SnakeActorMenu->LastMoveDirection = EMovementDirection::LEFT;
		}
	}
}

void APawnForMenu::HandlePlayerUpDownInput(float value)
{
	if (IsValid(SnakeActorMenu))
	{
		if (value > 0 && SnakeActorMenu->LastMoveDirection != EMovementDirection::DOWNER)
		{
			SnakeActorMenu->LastMoveDirection = EMovementDirection::UPPER;
		}
		else if (value < 0 && SnakeActorMenu->LastMoveDirection != EMovementDirection::UPPER)
		{
			SnakeActorMenu->LastMoveDirection = EMovementDirection::DOWNER;
		}
	}
}





