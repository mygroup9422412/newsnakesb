// Fill out your copyright notice in the Description page of Project Settings.


#include "StartGameFood.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "SnakeGame\SnakeBase.h"

// Sets default values
AStartGameFood::AStartGameFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AStartGameFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AStartGameFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AStartGameFood::Interact(AActor* Inteactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Inteactor);
		if (IsValid(Snake))
		{
			UGameplayStatics::OpenLevel(GetWorld(), FName("SnakeGameMap"));
		}
	}
}

