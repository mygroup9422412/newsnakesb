// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Food.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "SnakeElementBase.h"
#include "Components/InputComponent.h"
#include "Math/UnrealMathUtility.h"
#include "FoodDecrise.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
	AddFoodElements();
	AddPropsElements();
	AddFoodDecriseElements();
	SnakeActor->Geteaten();
	/*
	if (GEngine)
	{
		FString message = FString::Printf(TEXT("Value of variable HowMuchFoodEaten: %d"), SnakeActor->HowMuchFoodEaten);
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Yellow, message);
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Yellow, FString::Printf(TEXT("ASnakeBase::Geteaten()")));
	}	
	if (GEngine)
	{
		FString message = FString::Printf(TEXT("Value of variable HowMuchFood: %d"), HowMuchFood);
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Yellow, message);
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Yellow, FString::Printf(TEXT("ASnakeBase::Geteaten()")));
	}
	*/
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FTransform(SnakeHeadCameraLocation);
	/*
	if (GEngine)
	{
		FString message = FString::Printf(TEXT("Value of variable HowMuchFoodEaten: %d"), SnakeActor->HowMuchFoodEaten);
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Yellow, message);

	}
	if (GEngine)
	{
		FString message = FString::Printf(TEXT("Value of variable HowMuchFood: %d"), HowMuchFood);
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Yellow, message);

	}
	*/
	if (IsValid(SnakeActor) && SnakeActor->Geteaten() == HowMuchFood)
	{
		for (int i = 0; i < HowMuchFood; ++i)
		{
			FVector raf;
			raf.X = FMath::RandRange(-1000.0f, 1000.0f);
			raf.Y = FMath::RandRange(-1000.0f, 1000.0f);
			raf.Z = FMath::RandRange(0.0f, 2000.0f);
			FoodElements[i] = GetWorld()->SpawnActor<AFood>(FoodActorClass, raf, FRotator::ZeroRotator);


		}
		SnakeElementsProps.SetNum(HowMuchProps);
		for (int i = 0; i < HowMuchProps; ++i)
		{
			FVector raf;
			raf.X = FMath::RandRange(-3000.0f, 3000.0f);
			raf.Y = FMath::RandRange(-3000.0f, 3000.0f);
			raf.Z = FMath::RandRange(0.0f, 2000.0f);
			SnakeElementsProps[i] = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeActorPropsClass, raf, FRotator::ZeroRotator);

		}
		FoodDecriseElements.SetNum(HowMuchFoodDecrise);
		for (int i = 0; i < HowMuchFoodDecrise; ++i)
		{
			FVector raf;
			raf.X = FMath::RandRange(-3000.0f, 3000.0f);
			raf.Y = FMath::RandRange(-3000.0f, 3000.0f);
			raf.Z = FMath::RandRange(0.0f, 2000.0f);
			FoodDecriseElements[i] = GetWorld()->SpawnActor<AFoodDecrise>(FoodDecriseActorClass, raf, FRotator::ZeroRotator);

		}

		SnakeActor->PlusValueHowMuchFoodEaten();
		SetActorRotation(FRotator(-35, 0, 0));

		SetActorLocation(FVector(-5000, -140, 4682));
	}

	if (IsValid(SnakeActor) && SnakeActor->Geteaten() == ((HowMuchFood * 2) - 1))
	{
		SnakeActor->PlusValueHowMuchFoodEaten();
		for (int i = 0; i < HowMuchFood; ++i)
		{
			FVector raf;
			raf.X = FMath::RandRange(-1000.0f, 1000.0f);
			raf.Y = FMath::RandRange(-1000.0f, 1000.0f);
			raf.Z = FMath::RandRange(0.0f, 2000.0f);
			FoodElements[i] = GetWorld()->SpawnActor<AFood>(FoodActorClass, raf, FRotator::ZeroRotator);


		}
	}

	if (IsValid(SnakeActor) && SnakeActor->Geteaten() > ((HowMuchFood*2)))
	{
		for (int i = 0; i < SnakeElementsProps.Num(); i++)
		{
			ASnakeElementBase* SnakeElement = SnakeElementsProps[i];
			float Oscillation = FMath::Sin(GetWorld()->GetTimeSeconds() * 0.5f); 
			float XOffset = Oscillation * 5.0f; 
			FVector NewLocation = SnakeElement->GetActorLocation();
			NewLocation.X += XOffset;
			SnakeElement->SetActorLocation(NewLocation);
			FRotator Rotation = FRotator(0, Oscillation * 360, 0); 
			SnakeElement->SetActorRotation(Rotation);
		}
	}
	if (IsValid(SnakeActor) && SnakeActor->Geteaten() > ((HowMuchFood * 3)+1))
	{
		UGameplayStatics::OpenLevel(GetWorld(), FName("snakeMenuLvl"));
	}
}

void APlayerPawnBase::EatenBool()
{
	
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical",this,&APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
	PlayerInputComponent->BindAxis("Deep", this, &APlayerPawnBase::HandlePlayerUpDownInput);
	PlayerInputComponent->BindAxis("boost", this, &APlayerPawnBase::HandlePlayerBoostInput);
}



void APlayerPawnBase::AddFoodElements()
{;
if (IsValid(SnakeActor) && SnakeActor->Geteaten() <= HowMuchFood)
{
	FoodElements.SetNum(HowMuchFood);
	for (int i = 0; i < HowMuchFood; ++i)
	{
		FVector raf;
		raf.X = FMath::RandRange(-1000.0f, 1000.0f);
		raf.Y = FMath::RandRange(-1000.0f, 1000.0f);
		raf.Z = FMath::RandRange(0.0f, 0.0f);
		FoodElements[i] = GetWorld()->SpawnActor<AFood>(FoodActorClass, raf, FRotator::ZeroRotator);
	}
}
	if (IsValid(SnakeActor) && SnakeActor->Geteaten() == HowMuchFood)
	{

			for (int i = 0; i < HowMuchFood; ++i)
			{
				FVector raf;
				raf.X = FMath::RandRange(-3000.0f, 3000.0f);
				raf.Y = FMath::RandRange(-3000.0f, 3000.0f);
				raf.Z = FMath::RandRange(0.0f, 0.0f);
				FoodElements[i] = GetWorld()->SpawnActor<AFood>(FoodActorClass, raf, FRotator::ZeroRotator);
			}
			
	}


}

void APlayerPawnBase::AddPropsElements()
{
	SnakeElementsProps.SetNum(HowMuchProps);
	for (int i = 0; i < HowMuchProps; ++i)
	{
		FVector raf;
		raf.X = FMath::RandRange(-3000.0f, 3000.0f);
		raf.Y = FMath::RandRange(-3000.0f, 3000.0f);
		raf.Z = FMath::RandRange(0.0f, 0.0f);
		SnakeElementsProps[i] = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeActorPropsClass, raf, FRotator::ZeroRotator);
	}
}

void APlayerPawnBase::AddFoodDecriseElements()
{
	FoodDecriseElements.SetNum(HowMuchFoodDecrise);
	for (int i = 0; i < HowMuchFoodDecrise; ++i)
	{
		FVector raf;
		raf.X = FMath::RandRange(-3000.0f, 3000.0f);
		raf.Y = FMath::RandRange(-3000.0f, 3000.0f);
		raf.Z = FMath::RandRange(0.0f, 0.0f);
		FoodDecriseElements[i] = GetWorld()->SpawnActor<AFoodDecrise>(FoodDecriseActorClass, raf, FRotator::ZeroRotator);

	}
}


void APlayerPawnBase::CreateSnakeActor()
{
	
		SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}


/*void APlayerPawnBase::CreateFoodActor()
{
	for (int i = 0; i < HowMuchFood; ++i)
	{
		FoodElements[i]=GetWorld()->SpawnActor<AFood>(FoodActorClass, FTransform());
	}

}
*/






void APlayerPawnBase::SnakeDash()
{

}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0&& SnakeActor->LastMoveDirection!=EMovementDirection::DOWN)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
		}
	}
}

void APlayerPawnBase::HandlePlayerUpDownInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWNER)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UPPER;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UPPER)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWNER;
		}
	}
}

void APlayerPawnBase::HandlePlayerBoostInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0)
		{
			FVector a = SnakeActor->SnakeDash();
			//FVector BoostLocation = SnakeActor->SnakeHeadBoostLocat;
			SnakeActor->SnakeElements[0]->SetActorLocation(a);
		}
	}
}


